# README #

* Requirements
	* Create a big screen Hulu-style interface ( think minimal styling, we are focusing on the Javascript for now, styling can come later )
	* Create a server-side NodeJS app using the Express.js framework to handle the OCAPI API calls server side
	* You can only use OCAPI Shop API resources that require no authentication ( Categories, ProuctSearch, Products, SearchSuggestion )
	* Start with the “root” category, display subcategories and let the users traverse the categories. Display the category images and names at the top of the page.
	* Once you have the category navigation working, start displaying the product names, images and prices underneath the selected category


### What is this repository for? ###

* Using the OCAPI  (Open Commerce API) Shop API with Node

### How do I get set up? ###

* git clone https://tbui_lyonscg@bitbucket.org/tbui_lyonscg/ocapicalls.git
* run nodemon server.js localhost 3000
* http://localhost:3000/api/

### Who do I talk to? ###

* Repo owner or admin
* LyonsU Coordinator