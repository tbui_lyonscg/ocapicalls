const rp = require('request-promise-native')
const express = require('express')

const router = express.Router()

// To use to build URLs
const baseURL = "http://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_2/"

const baseProd_search = "product_search?expand=images,prices&q="
const baseProduct = "products/"

const baseCat_root = "categories/"
const cat_levels = "?levels="

const baseSuggestion = "search_suggestion?q="
const count = "&count="
const currency = "&currency="

const client_id = "client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

let SESSIONS = {}
let myCatalogs = []
let myProducts = {}

// request object
router.param('id', function(req, res, next) {
	var id = req.params.id
	req.id = id

	next()
})

// var token = "YWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhOmFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYQ=="

// TODO Link catalog and subcatalog, Clean up redundency (GET RID OF myCatalog)
// Home page
router.route('/')
.get(function(req, res) {
	session(req, res);

	rp(baseURL+baseCat_root+"root"+"?levels="+"2"+"&"+client_id)
	.then(function(body){
		let my_json = JSON.parse(body)
		let myImages = [], keyWords, images = []
		// Go through each product object and saving it separately
		for (let i = 0; i < my_json.categories.length; i++){
			myCatalogs[i] = my_json.categories[i]
			myImages[i] = my_json.categories[i]

			if (my_json.categories[i].page_keywords != undefined) { // page_keywords is a string
				keyWords = my_json.categories[i].page_keywords.split(',')	
			}
			else keyWords = ""
			my_json.categories[i].page_keywords = keyWords

			if (myImages[i].categories !== undefined)
				images[i] = myImages[i]
		}
		res.render('home', { images: images, subCatagories: myCatalogs })
	})
	.catch(function(err){
		console.log(err)
	})
})

router.route('/cart/form')
.get(function(req, res) {
	res.render('form')
})

router.route('/cart')
.get(function(req, res) {
	var cookies = req.headers.cookie;
	if (!cookies) res.redirect('/');

	//var session = SESSIONS[getSessionId(cookies)];
	//var ids = session.productsInCart.toString();

	res.render('shoppingcart')
})

// TODO search by product name instead of product id
// TODO suggestion incomplete
router.route('/:id')
.get(function(req, res) {
	if (req.query['searchquery'] != null) { // if searching whole product by keyword
		rp(baseURL+baseProd_search+req.query['searchquery']+"&"+client_id)
		.then(function(body){
			myProducts = JSON.parse(body)
			res.render('products', { products: JSON.parse(body), subCatagories: myCatalogs })
		})
		.catch(function(err){
			console.log(err)
		})
	} else if (req.query['product'] != null) { // if searching a product by keyword
		rp(baseURL+baseProduct+req.query['product']+"?expand=prices,images"+"&"+client_id)
		.then(function(body){
			let my_json = JSON.parse(body)
			let myImages = [], images = []
			// Go through each product object and saving it separately
			for (let i = 0; i < my_json.image_groups.length; i++){
				myImages[i] = my_json.image_groups

				images[i] = myImages[i][0].images[0].dis_base_link
			}
			console.log(req.query['product'])
			res.render('pdp', { singleProduct: JSON.parse(body), image: images, subCatagories: myCatalogs })
		})
		.catch(function(err){
			console.log(err)
		})
	} else if (req.query['suggestion'] != null) { // suggestion by enter keyword
		rp(baseURL+baseSuggestion+req.query['suggestion']+"&count=1&currency=USD&"+client_id)
		.then(function(body){
			res.render('suggestion', { suggested: JSON.parse(body), subCatagories: myCatalogs })
		})
		.catch(function(err){
			console.log(err)
		})
	}
})

router.route('/info/:id') // TODO //////////////////////////
.get(function(req, res){
	rp(baseURL+baseProduct+req.query['product']+"?expand=prices,images"+"&"+client_id)
	.then(function(body){
		let my_json = JSON.parse(body)
		let myImages = [], images = []
		// Go through each product object and saving it separately
		console.log(req.query['product'])
		for (let i = 0; i < my_json.image_groups.length; i++){
			myImages[i] = my_json.image_groups

			images[i] = myImages[i][0].images[0].dis_base_link
		}
		console.log(body)
		res.json(body)
	})
	.catch(function(err){
		console.log(err)
	})
})




// Making a session -------------> Evans Project 2
// Creating a cookie
function setCookie() {
	//var cookie = req.cookies.cookieName
	var date = new Date()
	date.setTime(date.getTime() + (24*60*60*1000))
	var expries = 'expires=' + date.toUTCString();
	//document.cookie = idname + '=' + idvalue + ';' + expries + ';path=/' 
	//if (cookie === undefined) {
	var randomNumber = Math.random().toString()
	randomNumber = randomNumber.substring(2, randomNumber.length)
		//res.cookie(`cookieName=${randomNumber};${expries}+`, { maxAge:900000, httpOnly: true })
	//} else console.log('cookie exists', cookie)
	return randomNumber
}

// Our session
function session(req, res) {
	//if(!req.headers.cookie) {
		var sessionId = setCookie();
		SESSIONS[sessionId] = { id : sessionId, productsInCart : [], numProducts : 0};
		console.log(sessionId)
		res.set('Set-Cookie', "session=" + sessionId);
	//}
}

// TODO COOKIES FOR CARTS


module.exports = router