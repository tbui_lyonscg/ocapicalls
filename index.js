'use strict'

const path = require('path')
const exphbs = require('express-handlebars')
const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const router = require('./routers/router')

const port = process.env.PORT || 3000

app.engine('handlebars', exphbs({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')
app.set('views', path.join(__dirname, 'views')) // View engine setup
app.use(express.static(path.join(__dirname, 'public')))

// To prevent errors from Cross Origin Resource Sharing, we will set our headers to allow CORS with middleware like so:
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Credentials', 'true')
	res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')
	res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')

	next()
})

app.use(express.static(path.join(__dirname, 'public'))) // Static files to be loaded
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use('/api', router)
app.listen(port, function() {
	console.log(`Server is running on localhost:${port}`)
})